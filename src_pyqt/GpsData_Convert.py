class GpsData_Convert:
  def __init__(self):
    return


# Busca un dato dentro del CSV con el formato de latitud o lingitud y lo
# convierte de formato. Si existe otro dato que finalice en N,S,W o E,
# tambien lo convertira.
  def search(self, data):
    CARDINALES = ['N', 'S', 'W', 'E']
    
    data=data.split(",")
    for index in range(len(data)):
      match = next((x for x in CARDINALES if x in data[index][-1:]), False)
      if match != False:
        data[index]=self.convert(data[index])
    data = ','.join(data)
    return data


# Convierte el dato enviado como parametro. Este puede ser una latitud o
# una longitud.
  def convert(self, unaLatitud):
    if (unaLatitud.find('S') != -1 or unaLatitud.find('W') != -1):
      unaLatitud=unaLatitud.strip('S')
      unaLatitud=unaLatitud.strip('W')
      signo = -1
    elif (unaLatitud.find('N') != -1 or unaLatitud.find('E') != -1):
      unaLatitud=unaLatitud.strip('N')
      unaLatitud=unaLatitud.strip('E')
      signo = 1
    else:
      return 'Formato de dato invalido'
    
    unaLatitud = float(unaLatitud)/100
    parteDecimal = unaLatitud-int(unaLatitud)
    parteDecimal = parteDecimal/60*100
    unaLatitud = int(unaLatitud)+parteDecimal
    unaLatitud = unaLatitud*signo

    return "%.4f" % (unaLatitud, )
