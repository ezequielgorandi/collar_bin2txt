import sys
import zlib
from GpsData_Convert  import GpsData_Convert
from PyQt4 import QtCore,QtGui
from PyQt4.QtGui import *

app = QtGui.QApplication([])
msg = QMessageBox()
msg.setWindowTitle("Gps Data Converter")
archivo=QtGui.QFileDialog.getOpenFileName(None,'choose the file','',str("Data (*.bin)"))

try:
  with open(archivo,"rb") as f:
     datosComprimidos = f.read()
except IOError as e:
    msg.setIcon(QMessageBox.Critical)
    msg.setText("Archivo no seleccionado")
    msg.setInformativeText(archivo)
    msg.setStandardButtons(QMessageBox.Ok)
    msg.exec_()
    sys.exit(0)

datosDescomprimidos = zlib.decompress(datosComprimidos)
lineas=datosDescomprimidos.splitlines()

for indice in range(len(lineas)):
  lineas[indice]=GpsData_Convert().search(lineas[indice])

datosConvertidos = '\n'.join(lineas)
archivo = str(archivo).strip(".bin")+".txt"
open(archivo,"w").write(datosConvertidos)

msg.setIcon(QMessageBox.Information)
msg.setText("Conversión exitosa!")
msg.setInformativeText(archivo)
msg.setStandardButtons(QMessageBox.Ok)
msg.exec_()
